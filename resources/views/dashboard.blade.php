@extends('root')

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}

                 
                    <h1>Générateur de commentaires Swagger</h1>

          <section>
               <h2>Contenu de requête reçu</h2>
               <?php print_r($_POST)?>
          </section>

          <section>
               <h2>Formulaire pour décrire l'utilisation de l'url</h2>
               <form action="#" method="POST">
                    <div class="field">
                         <label for="path" required>path :</label>
                         
                         <input id="path" name="path" type="text" value="<?php isset($_POST['path']) ? print_r($_POST['path']) : '' ?>" required />
                    </div>

                    <div class="field">
                         <label for="operationId">ID :</label>
                         <input id="operationId" name="operationId" type="text" value="<?php isset($_POST['operationId']) ? print_r($_POST['operationId']) : '' ?>" />
                    </div>
                    
                    <div class="field">
                         <label for="tags">Tag de groupement</label>
                         <input id="tags" name="tags" type="text" value="<?php isset($_POST['tags']) ? print_r($_POST['tags']) : '' ?>" />
                    </div>

                    <div class="field">
                         <label for="summary">Description courte</label>
                         <input id="summary" name="summary" type="text" value="<?php isset($_POST['summary']) ? print_r($_POST['summary']) : '' ?>" />
                    </div>

                    <div class="field">
                         <label for="description">Description détaillée</label>
                         <textarea id="description" name="description"><?php isset($_POST['description']) ? print_r($_POST['description']) : '' ?></textarea>
                    </div>

                    <fieldset>
                         <legend>Paramètre d'url</legend>
                         <div class="field">
                              <label for="urlparam_name">Name :</label>
                              <input id="urlparam_name" name="urlparam_name" type="text"/>
                         </div>

                         <div class="field">
                              <label for="pseudo">Description :</label>
                              <input id="pseudo" name="idpseudo" type="text" />
                         </div>

                         <div class="field">
                              <label for="pseudo">Type :</label>
                              <input list="urlparam_type" name="urlparam_type" />
                              <datalist id="urlparam_type">
                                   <option value="number">number</option>
                                   <option value="text">text</option>
                              </datalist>
                         </div>

                         <div class="field">
                         <input type="checkbox" name="urlparam_required" id="urlparam_required" /> <label for="urlparam_required">Requis</label>
                         </div>
                    </fieldset>


                    <button type="submit">Générer</button>
               </form>
          </section>

          <section>
               <h2>Code Swagger généré</h2>

<?php
if(isset($_POST['urlparam_name'])) {
?>
<pre><code>
     /**
     * @OA\Post(
     *      path="<?php print_r($_POST['path']) ?>",
     *      operationId="<?php print_r($_POST['operationId']) ?>",
     *      tags={"<?php print_r($_POST['tags']) ?>"},
     *      summary="<?php print_r($_POST['summary']) ?>",
     *      description="<?php print_r($_POST['description']) ?>"
<?php
     if(!empty($_POST['pathField'])) {
?>
     *      @OA\Parameter(
     *          name="<?php print_r($_POST['urlparam_name']) ?>",
     *          description="<?php print_r($_POST['urlparam_descript']) ?>",
     *          required=<?php print_r($_POST['urlparam_required']) ?>,
     *          in="path",
     *          @OA\Schema(
     *              type="<?php print_r($_POST['urlparam_type']) ?>",
     *          )
     *      ),
<?php
}
?>
     *      @OA\RequestBody(
     *          required=true,
     *          @OA\MediaType(mediaType="application/json",
     *              @OA\Schema(
     *                  required={"email", "horodatage", "mensualite_souhaitee", "duree_credit", "tax_credit", "apport_personnel", "capital_empruntable"},
     *                  @OA\Property(
     *                      property="email",
     *                      type="string",
     *                      description="Adresse email valide",
     *                      example="demo@digitalcooker.fr"
     * 
     *                  )
     *             )
     *         )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Succès",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Token non présent ou invalide",
     *          @OA\JsonContent()
     *      ),
     *      @OA\Response(
     *          response=422,
     *          description="Paramètres invalides",
     *          @OA\JsonContent()
     *      ),
     * )
     * 
<?php
     if(!empty($_POST['pathField'])) {
?>
     * @param  int  $<?php print_r($_POST['urlparam_name']) ?>
<?php
}
?>
     * @param StoreBudgetRequest $request
     * @return \Illuminate\Http\Response
     */
</code></pre>

<?php
}
?>
          </section>
          

          <section>
               <h2>Code de Request généré</h2>
          </section>



                </div>
            </div>
        </div>
    </div>
</x-app-layout>
